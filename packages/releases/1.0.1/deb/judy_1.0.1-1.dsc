-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: judy
Version: 1.0.1-1
Binary: libjudy-dev, libjudydebian1
Maintainer: Troy Heber <troy.heber@hp.com>
Architecture: any
Standards-Version: 3.6.1.1
Build-Depends: debhelper (>> 3.0.0)
Uploaders: Al Stone <ahs3@debian.org>
Files: 
 42fbf2e6ba0d7cfff6222a8e6610fe47 728816 judy_1.0.1-1.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.5 (GNU/Linux)

iD8DBQFBxvE/QOr9C+GfGI4RAqiMAJ9uHoQxNjIUXRTMz1D0ywgN6Z4JzgCaAs2a
1ET803l1aDTok0anpQop2u0=
=zYMj
-----END PGP SIGNATURE-----
