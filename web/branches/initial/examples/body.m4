define([[BODY_HTML]],[[
  <!--- BEGIN First Tables -->
  <td valign="top" width="98%">
  <table align="left" border="0" cellspacing="15" cellpadding="0" width="100%" summary="Center Table">

<tr>
   <td width="15"></td>
   <td align="Left" valign="top">
   <img src="/graphics/examples_bnr.jpg" name="Judy Usage Examples" alt="Judy Usage Examples"></td>
</tr>

<tr>
<td width="15"></td>
<td>
	<p>
	  <h3>Judy Usage Examples</h3>
	  The author attempted to write interesting application notes using
	  advanced features of Judy. However, some may be a little out of date. 
	  </p><p>
	  <ul>

	  <li><A href="Judy_hashing.pdf">Judy_hashing.pdf</a>
	  <br>
	  How to use Judy to create a scalable hash table with outstanding
	  performance and automatic scaling, while avoiding the complexity of
	  dynamic hashing.
	  </p><p>
	  </li>

	  <li><A href="content_addressable_memory.pdf">content_addressable_memory.pdf</a>
	  <br>
	  Determine which of a set of variable-size objects residing in memory
	  contains a specific address. 
	  </p><p>
	  </li>
	  
	  <li><A href="judysl.pdf">judysl.pdf</a>
	  <br>
	  Intro to how JudyL was used to create an associative array (JudySL).
	  </p><p>
	  </li>
	  
	  <li><A href="count_by_value.pdf">count_by_value.pdf</a>
	  <br>
	  How can the sum of a set of array values over a large, arbitray, spans
	  of data quickly be determined? That is, how can the sum be determined
	  faster than summing each individual value? 
	  </p><p>
	  </li>

	  <li><A href="design_rule_checking.pdf">design_rule_checking.pdf</a>
	  <br>
	  Given a set of rectangles, how quickly can you find all the rectangles
	  that touch any given rectangle?  Touch  means that if you considered
	  the rectangles to be solid, there would be at least one point in
	  common.
	  </p><p>
	  </li>

	  <li><A href="work_load_analyzer.pdf">work_load_analyzer.pdf</a>
	  <br>
	  How does disk cache size affect performance for accessing large (
	  terabyte or greater) disk units? Or at a more detailed level, how can
	  we rapidly count the number of unique disk I/O requests that take
	  place between any two requests for the same disk address?
	  </p><p>
	  </li>

	 </ul>

	</td>
  </tr>

  </table>
  </td>
  </tr>
  <!-- END First Tables -->
</table>
]])
