define([[START_HTML]],[[
<html>
  <head>
	<meta name="MSSmartTagsPreventParsing" content="TRUE">
	<title>Judy Arrays Web Page</title>
    
	
	<!--- BEGIN Style Sheet =-->
	<style type="text/css">
    	body {background-color: "#C6C6AD"; }
		 A{text-decoration:none}
    	h2.c3 {color: #ffffff; font-family: Helvetica, Arial, sans-serif; font-weight: bold}
    	div.c2 {text-align: center}
	b.c1 {color: #ffffff; font-family: Helvetica, Arial, sans-serif; font-size: 150%}
    	div.c3 {color: #ffffff; text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 140%}
    	div.c4 {color: #ffffff; font-family: Helvetica, Arial, sans-serif; font-size: 140%}
	</style>
    <!--- END Style Sheet =-->
  </head>
 <body bgcolor="#C6C6AD">
  
  

<!---
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	    <tr>
		<td align="center" valign="top">
		    <img src="/graphics/intro_bnr.jpg" name="Intro Banner" alt="Introduction"></td>
	    </tr>
	</table>
	<br>
=-->

  <table border="0" bgcolor="#C6C6AD" cellpadding="0" cellspacing="0" width="100%" summary="Whole Page Table">
      <tr>

   <!--- BEGIN Left Nav =-->
	  <td align="left" valign="top" width="200">
	      <table border="0" bgcolor="#7E1D13" width="100%" summary="Left Nav bar Center">

   <!--- Begin new nav box -->  
		  <tr>
		      <td>
		      <img src="/graphics/logo.png" name="Judy Logo" alt="Judy Logo"></td>
		  </tr>
		  <tr>
		      <td nowrap>
			  <a href="/index.html"><div class="c4">&nbsp &nbsp Introduction</div></a> 
		      </td>
		</tr> 
		<tr>
		    <td nowrap>
			<a href="/news/index.html"><div class="c4">&nbsp &nbsp News</div></a> 
		    </td>
		</tr> 
		<tr>
		    <td nowrap>
			<a href="/doc/index.html"><div class="c4">&nbsp &nbsp Documentation</div></a> 
		    </td>
		</tr> 
		<tr>
		    <td nowrap>
			<a href="/examples/index.html"><div class="c4">&nbsp &nbsp Examples</div></a> 
		    </td>
		</tr> 
		<tr>
		    <td nowrap>
			<a href="/downloads/index.html"><div class="c4">&nbsp &nbsp Downloads</div></a> 
		    </td>
		</tr> 
		<tr>
		    <td nowrap>
			<a href="/error/index.html"><div class="c4">&nbsp &nbsp Errors</div></a> 
		    </td>
		</tr> 
		<tr>
		    <td nowrap>
			<a href="/glossary/index.html"><div class="c4">&nbsp &nbsp Glossary</div></a> 
		    </td>
		</tr> 
		<tr>
		    <td nowrap>
			<a href="/contact/index.html"><div class="c4">&nbsp &nbsp Contact</div></a> 
		    </td>
		</tr> 
	    </tr>

  <!--- End nav box -->  

          </table>
        </td>
  <!--- END Left Nav =-->
]])

define([[END_HTML]],[[
  <!--  BEGIN Bottombanner Table -->
  <br>
  <br>
  <br>
  
  <table border="0" cellspacing="0" cellpadding="0" width="100%" summary="bottombanner information.">
      <tr>
	  <td width="1" nowrap>
	      <spacer type="BLOCK" width="1" height="1">
	  </td>
	  <td height="1" colspan="3" bgcolor="#7E1D13" nowrap>
	      <spacer type="BLOCK" width="1" height="1">
	  </td>
      </tr>

    
      <tr>
	  <td width="20" nowrap>
	      <spacer type="BLOCK" width="1" height="1">
	  </td>
	  <td nowrap valign="top" align="left" class="bottombanner">Modified: esyscmd([[date +"%D %r"]])
	  </td>
	   <td nowrap valign="top" align="center" class="bottombanner">
		    <a href="http://sourceforge.net"><img
		    src="http://sourceforge.net/sflogo.php?group_id=55753&amp;type=1"
		    width="88" height="31" border="0" alt="SourceForge.net Logo"
		    /></a></td>
	  <td align="right" class="bottombanner" valign="top">&copy;esyscmd([[date +"%Y"]]) Judy Team
	  </td>
      </tr>

      <tr>
	  <td height="25" colspan="4" nowrap><spacer type="BLOCK" width="1" height="1">
	  </td>
	  <td width="20" nowrap>
	      <spacer type="BLOCK" width="1" height="1">
	  </td>
      </tr>
  </table>
  
  <!--  END Bottombanner Table -->

  </body>
</html>
]])

